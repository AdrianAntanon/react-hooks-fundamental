import React, { useState, useEffect } from 'react';

import IngredientForm from './IngredientForm';
import IngredientList from './IngredientList';
import Search from './Search';

const FIREBASE_URL = 'https://react-hooks-fundamental-default-rtdb.firebaseio.com/ingredients.json';

const Ingredients = () => {
  const [userIngredients, setUserIngredients] = useState([]);

  useEffect(() => {
    fetch(FIREBASE_URL).then(
      response => response.json()
    ).then(responseData => {
      const loadedIngredients = [];
      for (const key in loadedIngredients) {
        loadedIngredients.push({
          id: key,
          title: responseData[key].title,
          amount: responseData[key].amount
        });
      }
      setUserIngredients(loadedIngredients);
    });
  }, []);

  useEffect(() => {
    console.log('RENDERING INGREDIENTS', userIngredients);
  }, [userIngredients]);

  const filteredIngredientsHandler = filteredIngredients => {
    setUserIngredients(filteredIngredients);
  };

  const addIngredientHandler = ingredient => {
    fetch('https://react-hooks-fundamental-default-rtdb.firebaseio.com/ingredients.json', {
      method: 'POST',
      body: JSON.stringify(ingredient),
      headers: { 'Content-Type': 'application/json' }
    }).then(response => {
      return response.json()
    }).then(responseData => {
      setUserIngredients(prevIngredients => [
        ...prevIngredients,
        { id: responseData.name, ...ingredient }
      ]);
    });
  }
  return (
    <div className="App">
      <IngredientForm
        onAddIngredient={addIngredientHandler}
      />

      <section>
        <Search
          onLoadIngredients={filteredIngredientsHandler}
        />
        <IngredientList
          ingredients={userIngredients}
          onRemoveItem={() => { }}
        />
      </section>
    </div>
  );
}

export default Ingredients;
